# !/usr/bin/env python
# -*-coding:utf-8 -*-

"""
# File       : FacialExpressionRecognizer.py
# Time       ：2022/7/20 16:26
# Author     ：沈冠翔
# version    ：python 3.8.10
# Description：
"""

import numpy as np
import os
from tensorflow import keras
import cv2
import config

# 去除红色的提示信息
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class FacialExpressionRecognizer:

    def __init__(self, model_path=config.model_path):
        """
        加载训练好的表情识别模型
        """
        print("从%s加载表情识别模型..." % (model_path,))
        if os.path.exists(model_path) and os.path.isfile(model_path):
            self.model = keras.models.load_model(model_path, custom_objects=None, compile=True, options=None)
            self.input_img_shape = self.model.layers[0].input_shape[1:3]  # 获取CNN输入层的row、col
            print("模型概要：")
            self.model.summary()
            # keras.utils.plot_model(self.model,
            #                        to_file='FER_CNN.png',
            #                        show_shapes=True,
            #                        show_dtype=True,
            #                        show_layer_names=True)
        else:
            print("模型路径出错")

    def predict_face_img(self, test_img):
        """
        :param test_img: 单张人脸图片
        :return: 表情识别网络分类得到的标签，以及该标签的置信概率
        """
        resized_img = cv2.resize(test_img, self.input_img_shape, interpolation=cv2.INTER_AREA)  # 首先裁剪/扩充原图
        x = np.expand_dims(np.array(resized_img) / 255.0, axis=0)  # 转化为ndarray、标准化、添加batch维度，以便作为CNN的输入
        pred_x = self.model.predict(x, verbose=1)
        class_x = np.argmax(pred_x, axis=1)
        return config.class_names[class_x[0]], pred_x[0][class_x[0]]

    def recognize_img(self, img, show=True, mark_confidence=True):
        """
        :param img: 单张图片
        :param show: 是否在程序中显示新图片
        :param mark_confidence: 是否标注置信概率
        :return: 将该图片标注好表情后得到的新图片
        将单张图片中的人脸表情信息标注出来
        """
        # 将图像转变成灰度图像，因为OpenCV人脸检测器需要灰度图像
        gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # 加载OpenCV人脸识别器
        face_cascade = cv2.CascadeClassifier(r"haarcascade_frontalface_alt2.xml")
        # scaleFactor表示每次图像尺寸减小的比例，minNeighbors表示构成检测目标的相邻矩形的最小个数
        # 这里选择图像尺寸减小1.1倍。minNeighbors越大，识别出来的人脸越准确，但也极易漏判
        faces = face_cascade.detectMultiScale(gray_img, scaleFactor=1.1, minNeighbors=2)
        for x, y, w, h in faces:
            # cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
            label, confidence = self.predict_face_img(img[y:y + w, x:x + h])
            text = label + '(' + str(round(confidence * 100, 2)) + '%)' if mark_confidence else label
            cv2.putText(img, text=text, org=(x, y), fontFace=cv2.FONT_HERSHEY_PLAIN, fontScale=1.5, color=(0, 255, 0),
                        thickness=2)
        if show:
            cv2.imshow('Result', img)
            cv2.waitKey(0)
            cv2.destroyAllWindows()
        return img

    def recognize_video(self, video_in_path, video_out_path='processed_video.mp4'):
        """
        :param video_in_path: 原始视频路径
        :param video_out_path: 处理后的视频的保存路径
        通过逐帧处理的方法将视频中的人脸表情信息标注出来
        """
        capture = cv2.VideoCapture(video_in_path)
        fps = capture.get(cv2.CAP_PROP_FPS)
        size = (int(capture.get(cv2.CAP_PROP_FRAME_WIDTH)),
                int(capture.get(cv2.CAP_PROP_FRAME_HEIGHT)))
        writer = cv2.VideoWriter(video_out_path, cv2.VideoWriter_fourcc(*'mp4v'), fps, size, True)
        if capture.isOpened():
            while True:
                ret, img = capture.read()
                if not ret:
                    break
                img = self.recognize_img(img, show=False, mark_confidence=False)
                writer.write(img)
        else:
            print('视频打开失败！')
        capture.release()
        writer.release()


if __name__ == '__main__':
    recognizer = FacialExpressionRecognizer()
    img1 = cv2.imread("images/happy1.jpg", flags=cv2.IMREAD_COLOR)
    recognizer.recognize_img(img1)
