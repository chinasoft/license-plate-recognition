# !/usr/bin/env python
# -*-coding:utf-8 -*-

"""
# File       : config.py
# Time       ：2022/7/20 14:48
# Author     ：沈冠翔
# version    ：python 3.8.10
# Description：相关配置
"""

# 文件路径，均为相对路径
model_path = 'FER_model.h5'
train_set = {
    'fer2013plus': 'datasets/fer2013plus/train',
    'jaffe': 'datasets/jaffe',
    'CK+': 'datasets/CK+',
    'AffectNet': 'datasets/AffectNet/train'
}
val_set = {
    'fer2013plus': 'datasets/fer2013plus/val',
    'jaffe': 'datasets/jaffe',
    'CK+': 'datasets/CK+',
    'AffectNet': 'datasets/AffectNet/val'
}
test_set = {
    'fer2013plus': 'datasets/fer2013plus/test',
    'jaffe': 'datasets/jaffe',
    'CK+': 'datasets/CK+',
    'AffectNet': 'datasets/AffectNet/test'
}


# CNN输出的分类结果

class_names = ['Angry', 'Disgusted', 'Fear', 'Happy', 'Sad', 'Surprised', 'Neutral', 'Contemptuous']
num_classes = len(class_names)

