# !/usr/bin/env python
# -*-coding:utf-8 -*-

"""
# File       : models.py
# Time       ：2022/7/20 15:34
# Author     ：沈冠翔
# version    ：python 3.8.10
# Description：创建表情识别模型并训练
"""

from tensorflow import keras
import matplotlib.pyplot as plt
import config
import os

# 去除红色的提示信息
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def create_model1():
    """
    改自AlexNet & LeNet5
    """
    model = keras.models.Sequential()

    # module 1
    model.add(keras.layers.Conv2D(256, kernel_size=(3, 3), input_shape=(48, 48, 3)))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation('relu'))
    model.add(keras.layers.Conv2D(256, kernel_size=(3, 3), padding='same'))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation('relu'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

    # module 2
    model.add(keras.layers.Conv2D(128, kernel_size=(3, 3), padding='same'))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation('relu'))
    model.add(keras.layers.Conv2D(128, kernel_size=(3, 3), padding='same'))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation('relu'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

    # module 3
    model.add(keras.layers.Conv2D(64, kernel_size=(3, 3), padding='same'))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation('relu'))
    model.add(keras.layers.Conv2D(64, kernel_size=(3, 3), padding='same'))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation('relu'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

    # flatten
    model.add(keras.layers.Flatten())

    # dense 1
    model.add(keras.layers.Dense(512))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation('relu'))

    # dense 2
    model.add(keras.layers.Dense(512))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation('relu'))

    # dense 3
    model.add(keras.layers.Dense(128))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation('relu'))

    # output layer
    model.add(keras.layers.Dense(config.num_classes, activation='softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer=keras.optimizers.Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-7),
                  metrics=['accuracy'])

    return model


def create_model2():
    """
    复现《A Real-time Facial Expression Recognizer using Deep Neural Network》,该论文在fer2013比赛中取得了最好的表现(准确率约70%)
    """
    model = keras.models.Sequential()

    # convolution 1
    model.add(keras.layers.Conv2D(filters=32, kernel_size=(5, 5), padding='same', input_shape=(42, 42, 3)))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation('relu'))

    # pooling 1
    model.add(keras.layers.MaxPooling2D(pool_size=(3, 3), strides=(2, 2)))

    # convolution 2
    model.add(keras.layers.ZeroPadding2D(padding=(1, 1)))
    model.add(keras.layers.Conv2D(filters=32, kernel_size=(4, 4)))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation('relu'))

    # pooling 2
    model.add(keras.layers.MaxPooling2D(pool_size=(3, 3), strides=(2, 2)))

    # convolution 3
    model.add(keras.layers.ZeroPadding2D(padding=(2, 2)))
    model.add(keras.layers.Conv2D(filters=64, kernel_size=(5, 5)))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation('relu'))

    # pooling 3
    model.add(keras.layers.MaxPooling2D(pool_size=(3, 3), strides=(2, 2)))

    # flatten
    model.add(keras.layers.Flatten())

    # fully-connected 1
    model.add(keras.layers.Dense(units=2048, kernel_regularizer=keras.regularizers.l2(0.001)))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation('relu'))
    model.add(keras.layers.Dropout(0.4))

    # fully-connected 2
    model.add(keras.layers.Dense(units=1024, kernel_regularizer=keras.regularizers.l2(0.001)))
    model.add(keras.layers.BatchNormalization())
    model.add(keras.layers.Activation('relu'))
    model.add(keras.layers.Dropout(0.4))

    # output layer
    model.add(keras.layers.Dense(config.num_classes, activation='softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer=keras.optimizers.Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-7),
                  metrics=['accuracy'])

    model.build()
    model.summary()

    return model


def img_rgb2bgr(img):
    """
    :param img:
    :return:
    将flow_from_directory的rgb格式转化为bgr格式，这样训练时也统一采用了bgr格式，使CNN的输入与opencv相对应
    """
    return img[:, :, :: -1]


def train_model(model, epochs=1, batch_size=64, src='fer2013plus', save_path=None, visualize=False):
    train_set = config.train_set[src]
    val_set = config.val_set[src]

    height = model.layers[0].input_shape[1]
    width = model.layers[0].input_shape[2]

    # 图片产生器，在批量中对数据进行增强，扩充数据集大小
    train_datagen = keras.preprocessing.image.ImageDataGenerator(
        rescale=1. / 255,
        featurewise_center=False,
        featurewise_std_normalization=False,
        rotation_range=40,
        width_shift_range=0.2,
        height_shift_range=0.2,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        fill_mode='nearest',
        preprocessing_function=eval('img_rgb2bgr')
    )

    train_generator = train_datagen.flow_from_directory(
        train_set,
        target_size=(height, width),
        batch_size=batch_size,
        seed=7,
        shuffle=True,
        class_mode='categorical'
    )

    valid_datagen = keras.preprocessing.image.ImageDataGenerator(
        rescale=1. / 255,
        preprocessing_function=eval('img_rgb2bgr')
    )
    valid_generator = valid_datagen.flow_from_directory(
        val_set,
        target_size=(height, width),
        batch_size=batch_size,
        seed=7,
        shuffle=True,
        class_mode='categorical'
    )

    train_num = train_generator.samples
    valid_num = valid_generator.samples

    es = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10, mode='min', restore_best_weights=True)

    history = model.fit(
        train_generator,
        steps_per_epoch=train_num // batch_size,
        epochs=epochs,
        verbose=1,
        callbacks=[es],
        validation_data=valid_generator,
        validation_steps=valid_num // batch_size
    )

    if save_path is not None:
        model.save(save_path)

    if visualize:
        fig, axes = plt.subplots(1, 2, figsize=(18, 6))
        # Plot training & validation accuracy values
        axes[0].plot(history.history['acc'])
        axes[0].plot(history.history['val_acc'])
        axes[0].set_title('Model accuracy')
        axes[0].set_ylabel('Accuracy')
        axes[0].set_xlabel('Epoch')
        axes[0].legend(['Train', 'Validation'], loc='upper left')

        # Plot training & validation loss values
        axes[1].plot(history.history['loss'])
        axes[1].plot(history.history['val_loss'])
        axes[1].set_title('Model loss')
        axes[1].set_ylabel('Loss')
        axes[1].set_xlabel('Epoch')
        axes[1].legend(['Train', 'Validation'], loc='upper left')
        plt.show()


# 模型评估函数
def evaluate_model(model, batch_size=64, src='fer2013plus'):
    test_set = config.test_set[src]
    height = model.layers[0].input_shape[1]
    width = model.layers[0].input_shape[2]

    test_datagen = keras.preprocessing.image.ImageDataGenerator(
        rescale=1. / 255,
        preprocessing_function=eval('img_rgb2bgr')
    )
    test_generator = test_datagen.flow_from_directory(
        test_set,
        target_size=(height, width),
        batch_size=batch_size,
        seed=7,
        shuffle=True,
        class_mode='categorical'
    )

    score = model.evaluate_generator(
        generator=test_generator,  # Generator yielding tuples
        steps=test_generator.samples // batch_size,
        # number of steps (batches of samples) to yield from generator before stopping
        max_queue_size=10,  # maximum size for the generator queue
        verbose=1
    )
    print("Performance on test_set %s: loss: %.6f - acc: %.6f" % (src, score[0], score[1]))


if __name__ == '__main__':
    base_model = create_model2()
    # base_model = keras.models.load_model('FER_model.h5')
    # print(base_model.layers[-1].units)
    # base_model.layers[-1].units = 8
    # print(base_model.layers[-1].units)
    # base_model.compile(loss='categorical_crossentropy',
    #                    optimizer=keras.optimizers.Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-7),
    #                    metrics=['accuracy'])
    train_model(base_model)
