# License Plate Recognition

## File Structure

### Movie_Character_Detection

#### 		1. Face_detection.py

#### 		2. Feelings_Predict.py

#### 		3. Pose_Detection.py

#### 		4. Gender_Age_Predict.py

#### 		5. Movie_Character_detection.py（main）

#### 		6. Model

##### 					6.1. coco

##### 					6.2. Gender_Age

#### 		7. Photos

##### 					7.1. Mike

##### 					7.2. Jack

##### 					7.3. Mary

#### 		8. Videos

##### 					8.1. Video.mp4

#### 9. Yolo1.py

#### 10. Yolo2.py

.....